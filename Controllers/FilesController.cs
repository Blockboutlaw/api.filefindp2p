using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace API.FILEP2P.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FilesController : ControllerBase
    {
        
        // POST api/values
        [HttpPost]
        public DataTable Post([FromBody] JObject result)
        {
            dynamic stuff = JObject.Parse(result.ToString());
            Console.WriteLine(stuff.searchstring.ToString());

            string[] filetypes = {stuff.filetype1.ToString(), stuff.filetype2.ToString(), stuff.filetype3.ToString()};

            FileCollection FileCollection = new FileCollection();
            return FileCollection.getfilecollection(filetypes, stuff.searchstring.ToString());


        }

       
    }
}
