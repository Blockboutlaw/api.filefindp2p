using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

public class FileCollection{
  
    public DataTable getfilecollection(string[] fileTypes, string searchString){
        DataTable dt = new DataTable();
        try
        {
            // Set a variable to the My Documents path.
            string docPath =
            Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            foreach (string s in fileTypes)
            {
                
            var files = from file in Directory.EnumerateFiles(docPath, s, SearchOption.AllDirectories)
                        from line in File.ReadLines(file)
                        where line.Contains("Microsoft")
                        select new
                        {
                            File = file,
                            Line = line
                        };

            foreach (var f in files)
            {
                dt.Rows.Add(f.File, f.Line);
            }
            
            }
        }
        catch (UnauthorizedAccessException uAEx)
        {
            Console.WriteLine(uAEx.Message);
        }
        catch (PathTooLongException pathEx)
        {
            Console.WriteLine(pathEx.Message);
        }
    return dt;
    }

    public DataTable getfilecollection(string[] fileTypes, string searchString, int maxNumResults){
        throw new RowNotInTableException();
    }




}